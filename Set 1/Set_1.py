############################################################################################################################################################
###########
#Exercise 1
###########
import planetary_systems_toolbox as pstool
import numpy as np
import matplotlib.pyplot as plt
import math as m
J2 = 0.00108263
R_e = 6378.13646 #km
GM = 3.986004418*10**(5) # km^3 s^-2 #
radian = np.pi/180
degrees = 180/np.pi

SMA = 10000 #km
ECC = 0.1
INC = [5*i for i in range(1,36) ] #deg
RAAN = 45 #deg
AOP = 45 #deg
TA = 0.00001 #deg, math domain error in the pstool J2 propagator if = 0

#Numerical calculation of Omega and omega derivatives
Days_to_propagate = 1 #days
Time_step = 0.001 #days

Odots_numerical = []
odots_numerical = []
for i in range(len(INC)):
    keplerian_elements = np.array([SMA,ECC,INC[i]*radian,RAAN*radian,AOP*radian,TA*radian])
    state_vector = pstool.keplerian_to_cartesian(keplerian_elements,GM)
    positions,period,orbital_elements,solution = pstool.J2_propagator(state_vector,Days_to_propagate,Time_step,GM)
    m,b = pstool.fit_element(solution,GM,Time_step,elem='RAAN')
    Odots_numerical.append(m)
    j,k = pstool.fit_element(solution,GM,Time_step,elem='AOP')
    odots_numerical.append(j)

#Analytical calculation of Omega and omega derivatives
Odots_formula = []
odots_formula = []

for i in range(len(INC)):
    Odots = -3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2 / (SMA**2*( 1 - ECC**2 )**2) * np.cos(INC[i]*radian)*86400
    Odots_formula.append(Odots)
    odots = 3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2/ (SMA**2*( 1 - ECC**2 )**2) * (2 - 5/2 * np.sin(INC[i]*radian)**2)*86400
    odots_formula.append(odots)

#Plots
plt.scatter(INC,[i*degrees for i in Odots_formula],label = 'RAAN analytical',c='red')
plt.plot(INC, Odots_numerical, label = "RAAN numerical",c='black')
plt.title("RAAN rates, a = 10,000km, e = 0.1")
plt.xlabel('Inclination (degrees)')
plt.ylabel('degrees/day')
plt.grid()
plt.legend()
plt.savefig("RAAN_rate.pdf", format="pdf", bbox_inches="tight")

plt.scatter(INC, [i*degrees for i in odots_formula],label='AOP analytical',c='red')
plt.plot(INC,  odots_numerical, label = "AOP numerical",c='black')
plt.title("AOP rates, a = 10,000km, e = 0.1")
plt.ylabel('degrees/day')
plt.grid()
plt.legend()
plt.savefig("AOP_rate.pdf", format="pdf", bbox_inches="tight")
############################################################################################################################################################
###########
#Exercise 2
###########
J2 = 0.00108263
R_e = 6378.13646 #km
GM = 3.986004418*10**(5) # km^3 s^-2 #

radian = np.pi/180
degrees = 180/np.pi

#Swap ECC and SMA values respectively 
ECC = 0.1
INC = [2*i for i in range(1,89) ] #deg
RAAN = 45 #deg
AOP = 45 #deg
TA = 0.0000 #deg

#Analytical calculation of Omega and omega derivatives
Odots_0 = []
odots_0= []
SMA = 9500

for i in range(len(INC)):
    Odots = -3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2 / (SMA**2*( 1 - ECC**2 )**2) * np.cos(INC[i]*radian)*86400
    Odots_0.append(Odots)
    odots = 3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2/ (SMA**2*( 1 - ECC**2 )**2) * (2 - 5/2 * np.sin(INC[i]*radian)**2)*86400
    odots_0.append(odots)

Odots_01 = []
odots_01= []
SMA = 10000


for i in range(len(INC)):
    Odots = -3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2 / (SMA**2*( 1 - ECC**2 )**2) * np.cos(INC[i]*radian)*86400
    Odots_01.append(Odots)
    odots = 3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2/ (SMA**2*( 1 - ECC**2 )**2) * (2 - 5/2 * np.sin(INC[i]*radian)**2)*86400
    odots_01.append(odots)

Odots_02 = []
odots_02 = []
SMA = 10500
for i in range(len(INC)):
    Odots = -3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2 / (SMA**2*( 1 - ECC**2 )**2) * np.cos(INC[i]*radian)*86400
    Odots_02.append(Odots)
    odots = 3/2 * np.sqrt(GM/SMA**3)*J2*R_e**2/ (SMA**2*( 1 - ECC**2 )**2) * (2 - 5/2 * np.sin(INC[i]*radian)**2)*86400
    odots_02.append(odots)

#Plots
plt.plot(INC,[i*degrees for i in Odots_0], label = "a = 9500")
plt.plot(INC, [i*degrees for i in Odots_01], label = "a = 10000")
plt.plot(INC, [i*degrees for i in Odots_02], label = "a = 10500")

plt.title("RAAN rates, e = 0.1")
plt.xlabel('Inclination (degrees)')
plt.ylabel('degrees/day')
plt.grid()
plt.legend()
plt.savefig("RAAN_rates_for_a.pdf", format="pdf", bbox_inches="tight")
plt.show()

plt.plot(INC,[i*degrees for i in odots_0], label = "a = 9500")
plt.plot(INC, [i*degrees for i in odots_01], label = "a = 10000")
plt.plot(INC, [i*degrees for i in odots_02], label = "a = 10500")

plt.title("AOP rates, e = 0.1")
plt.xlabel('Inclination (degrees)')
plt.ylabel('degrees/day')
plt.grid()
plt.legend()
plt.savefig("AOP_rates_for_a.pdf", format="pdf", bbox_inches="tight")
plt.show()
############################################################################################################################################################
###########
#Exercise 3
###########
R_e = 6378.13646 #km
J2 = 0.00108263
GM = 3.986004418*10**(5) # km^3 s^-2 #
degrees = 180/m.pi


SSO = 2*m.pi/(365.24*86400) #<---- Odot equal to this, condition for SSO

altitudes = [i*10 for i in range(20,201)]

SMA = [i+R_e for i in altitudes]
ECC = 0

#Analytical calculation of corresponding inclinations
SSO_INC = []
for i in range(len(SMA)):
    n = m.sqrt(GM/SMA[i]**3)
    INC = m.acos(  -2*SMA[i]**2*(1-ECC**2)**2 / (3*n*J2*R_e**2) * SSO  )
    SSO_INC.append(INC*degrees)


plt.scatter(SSO_INC,altitudes, label = "SSO, e = 0.0",s=1,c='black')
plt.title("Inclinations of SSOs for different altitudes")
plt.xlabel('Inclination (degrees)')
plt.ylabel('Altitude')
plt.grid()
plt.legend()
plt.savefig("SSOincs.pdf", format="pdf", bbox_inches="tight")
plt.show()