import numpy as np
import math as m
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def cartesian_to_keplerian(state_vector,GM):
    r = sum(state_vector[0:3]**2)**(1/2)
    v = sum(state_vector[3:6]**2)**(1/2)
    v_radial = sum(state_vector[3:6] * state_vector[0:3])/r
    energy = 1/2*v**2 - GM/r
    SMA = -GM/(2*energy)
    h = np.cross(state_vector[0:3],state_vector[3:6])
    INC = m.acos(h[2]/(sum(h**2)**(1/2)))
    node_line = np.cross(np.array([0,0,1]),h)
    if node_line[1] >= 0:
        RAAN = m.acos(node_line[0]/(sum(node_line**2))**(1/2)) 
    else:
        RAAN = 2*m.pi - m.acos(node_line[0]/(sum(node_line**2))**(1/2))
    eccentricity_vector = 1/GM * ( (v**2-GM/r)*state_vector[0:3] - r*v_radial*state_vector[3:6] )
    ECC = sum(eccentricity_vector**2)**(1/2)
    if eccentricity_vector[2] >= 0:
        AOP = m.acos(sum(node_line*eccentricity_vector)/(sum(node_line**2)**(1/2)*sum(eccentricity_vector**2)**(1/2)))
    else:
        AOP = 2*m.pi - m.acos(sum(node_line*eccentricity_vector)/(sum(node_line**2)**(1/2)*sum(eccentricity_vector**2)**(1/2)))
    if v_radial >= 0:
        TA = m.acos(sum(eccentricity_vector*state_vector[0:3])/(sum(eccentricity_vector**2)**(1/2)*sum(state_vector[0:3]**2)**(1/2)))
    else:
        TA = 2*m.pi - m.acos(sum(eccentricity_vector*state_vector[0:3])/(sum(eccentricity_vector**2)**(1/2)*sum(state_vector[0:3]**2)**(1/2)))
    keplerian_elements = np.array([SMA,ECC,INC,RAAN,AOP,TA])
    return(keplerian_elements)

def keplerian_to_cartesian(keplerian_elements,GM):
    p = keplerian_elements[0]*( 1 - keplerian_elements[1]**2)
    r = p / ( 1 + keplerian_elements[1] * m.cos( keplerian_elements[5] ) )
    r_perifocal = r*np.array([m.cos( keplerian_elements[5] ) , m.sin( keplerian_elements[5] )  , 0] )
    v_perifocal = (GM/p)**(1/2)*np.array([-m.sin(keplerian_elements[5]) , keplerian_elements[1] + m.cos(keplerian_elements[5]) , 0 ])
    R3_RAAN = np.array([[m.cos( keplerian_elements[3] ) , m.sin( keplerian_elements[3] ) , 0 ],
                        [-m.sin( keplerian_elements[3] ) , m.cos( keplerian_elements[3] ) , 0 ], 
                        [0 , 0 , 1] ])
    R1_INC = np.array([ [1 , 0 , 0   ],
                        [0 , m.cos( keplerian_elements[2] ) , m.sin(keplerian_elements[2]) ],
                        [0 ,-m.sin(keplerian_elements[2]) , m.cos(keplerian_elements[2]) ] ])
    R3_AOP = np.array([ [m.cos( keplerian_elements[4] ) , m.sin( keplerian_elements[4] ) , 0 ],
                        [-m.sin( keplerian_elements[4] ) , m.cos( keplerian_elements[4] ) , 0 ],
                        [0 , 0 , 1] ])
    intermediate_matrix = np.matmul(R3_RAAN.transpose(),R1_INC.transpose())
    T_PQW_ICRF = np.matmul(intermediate_matrix,R3_AOP.transpose())
    r_icrf = np.matmul(T_PQW_ICRF,r_perifocal)
    v_icrf = np.matmul(T_PQW_ICRF,v_perifocal)
    state_vector = np.concatenate((r_icrf,v_icrf))
    return(state_vector)

def true_to_mean(keplerian_elements):
    eccentric = 2*m.atan(((1-keplerian_elements[1])/(1+keplerian_elements[1]))**(1/2)*m.tan(keplerian_elements[5]/2))
    mean = eccentric - keplerian_elements[1]*m.sin(eccentric)
    return(mean)

def mean_to_true(keplerian_elements,mean):
    eccentric = mean
    dif = 1
    while dif > 10**(-9):
        eccentric_p1 = eccentric - (eccentric - keplerian_elements[1]*m.sin(eccentric) - mean)/(1 - keplerian_elements[1]*m.cos(eccentric))
        dif = abs(eccentric - eccentric_p1)
        eccentric = eccentric_p1
    true = 2*m.atan( ((1+keplerian_elements[1])/(1-keplerian_elements[1]))**(1/2)*m.tan(eccentric/2) )
    return(true)

def keplerian_propagator(state_vector,propagated_days,time_step,GM):
    positions = np.empty((0,3),float)
    kep_elem = cartesian_to_keplerian(state_vector,GM)
    n = 2*m.pi/( ( 4*m.pi**2/GM*kep_elem[0]**3 )**(1/2) )
    for i in np.arange(0,propagated_days,time_step):
        mean = true_to_mean(kep_elem)
        mean_next = mean +  n*time_step*86400
        true_next = mean_to_true(kep_elem,mean_next)
        kep_elem[5] = true_next
        state_vector_next = keplerian_to_cartesian(kep_elem,GM)  
        kep_elem = cartesian_to_keplerian(state_vector_next,GM)
        positions = np.append(positions,[state_vector_next[:3]],axis=0)
    return(positions,(2*np.pi/n)/60/60/24,kep_elem[:5])

def two_body_eqm(state_vector,t):
    GM = 3.986004418E+05
    x = state_vector[0]
    y = state_vector[1]
    z = state_vector[2]
    xdot = state_vector[3]
    ydot = state_vector[4]
    zdot = state_vector[5]
    r = x ** 2 + y ** 2 + z ** 2
    xddot = -GM * x / r ** (3 / 2)
    yddot = -GM * y / r ** (3 / 2)
    zddot = -GM * z / r ** (3 / 2)
    state_derivative = np.array([xdot,ydot,zdot,xddot,yddot,zddot])
    return(state_derivative)

def simple_propagator(state_vector,propagated_days,time_step,GM):
    positions = np.empty((0,3),float)
    t = np.arange(0, propagated_days*86400, time_step*86400)
    sol = odeint(two_body_eqm, state_vector, t)
    for i in range(len(sol)):
        pos_new = sol[i][:3]
        positions = np.append(positions,[pos_new],axis=0)
    kep_elem = cartesian_to_keplerian(state_vector,GM)
    period = (4*m.pi**2/GM * kep_elem[0]**3)**(1/2)
    return(positions,period/60/60/24,kep_elem[:5])

def display_results(positions,period,orbital_elements):
    au = 149597870.7
    degree_conversion = 180/np.pi
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    center_body = 6378 #km
    u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:20j]
    x = center_body*np.cos(u)*np.sin(v)
    y = center_body*np.sin(u)*np.sin(v)
    z = center_body*np.cos(v)
    ax.plot_surface(x, y, z, color="blue")
    ax.scatter(positions[:,0],positions[:,1],positions[:,2],s = 3,c = 'red')
    plt.axis('scaled')
    plt.show()

    print('\n','ORBITAL ELEMENTS:','\n','Semi major axis: ',round(orbital_elements[0]/au,5),
    ' AU','\n','Inclination: ',round(orbital_elements[2]*degree_conversion,5),' degrees','\n',
        'Eccentricity: ',round(orbital_elements[1],5),'','\n','Right ascension', 
        round(orbital_elements[3]*degree_conversion,5),' degrees','\n',
        'Argument of the periapsis',round(orbital_elements[4]*degree_conversion,5),
        ' degrees','\n','Orbital period: ',round(period,5),' days','\n')

def two_body_J2(state_vector,t):
    GM = 3.986004418E+05
    J2 = 0.00108263
    R = 6378.13646 #km
    x = state_vector[0]
    y = state_vector[1]
    z = state_vector[2]
    xdot = state_vector[3]
    ydot = state_vector[4]
    zdot = state_vector[5]
    r = (x ** 2 + y ** 2 + z ** 2)**(1/2)
    p_factor = 3/2 * J2*GM*R**2/r**4
    xddot = -GM * x / r ** 3 + p_factor*(x/r*(5*z**2/r**2-1))
    yddot = -GM * y / r ** 3 + p_factor*(y/r*(5*z**2/r**2-1))
    zddot = -GM * z / r ** 3 + p_factor*(z/r*(5*z**2/r**2-3))
    state_derivative = np.array([xdot,ydot,zdot,xddot,yddot,zddot])
    return(state_derivative)

def J2_propagator(state_vector,propagated_days,time_step,GM):
    positions = np.empty((0,3),float)
    t = np.arange(0, propagated_days*86400, time_step*86400)
    sol = odeint(two_body_J2, state_vector, t)
    for i in range(len(sol)):
        pos_new = sol[i][:3]
        positions = np.append(positions,[pos_new],axis=0)
    kep_elem = cartesian_to_keplerian(state_vector,GM)
    period = (4*m.pi**2/GM * kep_elem[0]**3)**(1/2)
    return(positions,period/60/60/24,kep_elem[:5],sol)

def draw_elements(solution,GM,elem):
    kep_elem = np.empty((0,6),float)
    for i in range(len(solution)):
        elems = cartesian_to_keplerian(solution[i],GM)
        kep_elem = np.append(kep_elem,[elems],axis=0)
    if elem == 'SMA':
        plt.title(elem+' over propagation')
        plt.ylabel('km')
        plt.plot(kep_elem[:,0])
        plt.show()
    elif elem == 'ECC':
        plt.title(elem+' over propagation')
        plt.plot(kep_elem[:,1])
        plt.show()
    elif elem == 'INC':
        plt.title(elem+' over propagation')
        plt.ylabel('degrees')
        plt.plot(kep_elem[:,2]*180/m.pi)
        plt.show()
    elif elem == 'RAAN':
        plt.title(elem+' over propagation')
        plt.ylabel('degrees')
        plt.plot(kep_elem[:,3]*180/m.pi)
        plt.show()
    elif elem == 'AOP':
        plt.title(elem+' over propagation')
        plt.ylabel('degrees')
        plt.plot(kep_elem[:,4]*180/m.pi)
        plt.show()
    elif elem == 'TA':
        plt.title(elem+' over propagation')
        plt.ylabel('degrees')
        plt.plot(kep_elem[:,5]*180/m.pi)
        plt.show()
    else:
        print('Enter valid orbital element abbreviation')

def fit_element(solution,GM,time_step,elem):
    kep_elem = np.empty((0,6),float)
    for i in range(len(solution)):
        elems = cartesian_to_keplerian(solution[i],GM)
        kep_elem = np.append(kep_elem,[elems],axis=0)
    t = np.arange(0,len(kep_elem)*time_step*86400,time_step*86400)
    if elem == 'RAAN':
        slope = (len(kep_elem)*sum(t*kep_elem[:,3]) - sum(t)*sum(kep_elem[:,3]))/(len(kep_elem)*sum(t**2)-sum(t)**2)
        intercept = (sum(kep_elem[:,3]) - slope*sum(t))/len(kep_elem)
    elif elem == 'AOP':
        slope = (len(kep_elem)*sum(t*kep_elem[:,4]) - sum(t)*sum(kep_elem[:,4]))/(len(kep_elem)*sum(t**2)-sum(t)**2)
        intercept = (sum(kep_elem[:,4]) - slope*sum(t))/len(kep_elem)
    return(slope*86400*180/m.pi,intercept)